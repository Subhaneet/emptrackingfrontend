import { Component, OnInit } from '@angular/core';
import {UserService} from '../user.service';
import {UserModal} from '../model/user';
import {FormArray, FormControl, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-admin-approval',
  templateUrl: './admin-approval.component.html',
  styleUrls: ['./admin-approval.component.css']
})
export class AdminApprovalComponent implements OnInit {

  user: any;

  constructor(private userService: UserService, private route: Router) { }

  getAllUsers() {
    this.userService.getAllUsers().subscribe(
      (data: any) => this.user = data.data
    );
  }

  // toggleAuth(form) {
  //   this.user = form;
  //   console.log(form);
  // }
  //
  // toggle(e, user) {
  //   e.preventDefault();
  //   console.log(e);
  //   if(window.confirm('are you sure?')){
  //     user.isAuth = e.target.checked;
  //     console.log(user);
  //     this.userService.updateUser(user).subscribe(
  //       (res: any) => {
  //         // const userIndex = this.user.findIndex(x => x.id == res.id);
  //         // if (userIndex > -1) {
  //         //     this.user[userIndex] = res;
  //         //   }
  //         console.log(res);
  //       }
  //     );
  //   } else {
  //     // e.preventDefault();
  //     // user.isAuth = !e.target.checked;
  //     // console.log(user.isAuth, e.target.checked);
  //   }
  // }

  approve(id) {
    this.userService.userApproval(id).subscribe(
      (res: any) => {
        if(res.message == "user active status updated") {
          this.route.navigate(['users']);
        }
      }
    );
  }

  disapproval(id) {
    this.userService.deleteUser(id).subscribe(
      res => console.log(res)
    );
  }

  ngOnInit() {
    this.getAllUsers();
  }
}
