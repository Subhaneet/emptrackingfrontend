import { Component, OnInit } from '@angular/core';
import {LoginModal} from '../login';
import {UserService} from '../user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  currentUserData: any;

  constructor(private userService: UserService, private route: Router) {
    if (userService.loggedIn) {
      route.navigate(['users']);
    }
  }

  ngOnInit() {
  }

  onSubmit(data: LoginModal) {
    this.userService.login(data).subscribe((response: any) => {
      console.log('user data', response );
      if(response.message == "user email correct") {
        localStorage.setItem('currentUser', 'loggedIn');
        this.route.navigate(['users'])
      }
    });
  }

  signUp() {
    this.route.navigate(['register']);
  }

  viewProfileEmitted(e){
    console.log(e);
  }
}
