import {Component, OnInit} from '@angular/core';
import {UserService} from '../user.service';
import {UserModal} from '../model/user';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {mustMatch} from '../Custom/MustMatch';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  userForm: FormGroup;
  submitted = false;

  user: any;

  constructor(private userService: UserService, private router: Router, private fb: FormBuilder) { }

  getUsers() {
    this.userService.getAllUsers().subscribe(
      (data:any) => {
        this.user = data.data;
      }
    );
  }

  ngOnInit() {
    this.userForm = this.fb.group(
      {
        id: new FormControl(''),
        user_name: new FormControl('', [
          Validators.compose([
            Validators.required,
          ])
        ]),
        user_email: new FormControl('', [
          Validators.compose([
            Validators.required,
            Validators.email,
            Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
          ])
        ]),
        user_phone: new FormControl('', [
            Validators.minLength(10),
            Validators.maxLength(10)
          ]
        ),
        user_password: new FormControl('', [
          Validators.required,
          Validators.minLength(8)
        ]),
        confirmPassword: new FormControl('', [
          Validators.required,
        ]),
        user_dob: new FormControl(new Date(), [
          Validators.required,
        ]),
        user_gender: new FormControl('', [
          Validators.required
        ])
        // imgPath: new FormControl('')
      }, {
        validator: mustMatch('user_password', 'confirmPassword')
      }
    );
    this.getUsers();
  }

  get userFormControl() {
    return this.userForm.controls;
  }

  addUser() {

    this.userService.addUser(this.userForm.value).subscribe(
      userData => {
        this.user = userData;
        this.router.navigate(['users']);
        console.log(this.user);
      }
    );
    this.getUsers();
  }

  onSubmit() {
    if (this.userForm.valid) {
      this.addUser();
    }
    this.submitted = true;
  }
}
