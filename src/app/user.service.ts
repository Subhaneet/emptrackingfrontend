import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {tap, catchError, map} from 'rxjs/operators';
import {UserModal} from './model/user';
import {LoginModal} from './login';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  newApi = `http://localhost:3040`;
  headers = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json');
  httpOptions = {
    headers: this.headers
  };

  constructor(private http: HttpClient) { }

  private static handleError(error: any) {
    console.log(error);
    catchError(error);
  }

  getAllUsers() {
    return this.http.get(`${this.newApi}/users`)
  }

  login(credentials) {
    console.log(credentials);
    return this.http.post(`${this.newApi}/login`, credentials).pipe(
      map (data=>data
      )
    );
  }

  logout() {
    localStorage.removeItem('currentUser');
  }

  public get loggedIn(): boolean {
    return (localStorage.getItem('currentUser') !== null);
  }

  getUser(id) {
    const url = `${this.newApi}/users/${id}`;
    return this.http.get(url).pipe(
      // @ts-ignore
      catchError(UserService.handleError)
    );
  }

  registerUser(registerDetails) {
    console.log(registerDetails);
    return this.http.post(`${this.newApi}/users`, registerDetails, this.httpOptions).pipe(
      tap (
        data => console.log(data)
      )
    );
  }

  addUser(user: any) {
    user.dob = new Date(user.dob);
    console.log(user);
    return this.http.post(`${this.newApi}/users`, user, this.httpOptions).pipe(
      tap(
        data => console.log(data)
      )
    );
  }

  // @ts-ignore
  deleteUser(id){
    const url = `${this.newApi}/users/${id}`;
    return this.http.delete(url, this.httpOptions).pipe(
      // @ts-ignore
      catchError(UserService.handleError)
    );
  }

  userApproval(id) {
    return this.http.get(`${this.newApi}/users/approve/${id}`).pipe(
      tap (
        data => data
      )
    )
  }

  updateUser(user: any) {
    const url = `${this.newApi}/users`;
    return this.http.put(url, user, this.httpOptions).pipe(
      map((data: any) => data),
      // @ts-ignore
        catchError(UserService.handleError)
    )
  }
}

