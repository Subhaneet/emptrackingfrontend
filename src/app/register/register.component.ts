import { Component, OnInit } from '@angular/core';
import {UserService} from '../user.service';
import {UserModal} from '../model/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerData: UserModal[];
  default = false;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  onSubmit(form) {
    console.log(form);
    this.userService.registerUser(form).subscribe(
      response => console.log(response)
    );
  }

}
