import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {UserComponent} from './user/user.component';
import {LoginComponent} from './login/login.component';
import {AuthGuardService} from './guards/auth-guard.service';
import {AdminApprovalComponent} from './admin-approval/admin-approval.component';
import {RegisterComponent} from './register/register.component';
import {ViewProfileComponent} from './view-profile/view-profile.component';

const routes: Routes = [
  {path: 'users', component: UserComponent},
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'approve', component: AdminApprovalComponent},
  {path: 'view-profile', component: ViewProfileComponent, canActivate: [AuthGuardService]}
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),
    CommonModule
  ]
})
export class AppRoutingModule { }
