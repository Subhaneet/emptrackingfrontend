import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {UserModal} from '../model/user';
import {UserService} from '../user.service';

@Component({
  selector: 'app-update-modal',
  templateUrl: './update-modal.component.html',
  styleUrls: ['./update-modal.component.css']
})
export class UpdateModalComponent implements OnInit {
  @Output() updateEditData = new EventEmitter();
  // @ViewChild('ModalBtn', {read: ElementRef, static: false}) modalBtn: ElementRef;
  userData: UserModal;
  isFormVisible = false;

  @ViewChild('CloseBtn', {read: ElementRef, static: false}) Closebtn: ElementRef;

  @Input() set userEditData(value: any) {
    if (value) {
      if(value && value.user_id)
        value.user_dob = value.user_dob.substr(0,10);
      this.userData = {...value};
    }
  }

  constructor(private userService: UserService) { }
  ngOnInit() {
  }

  updateUser(newUpdateData) {
    this.userService.updateUser(newUpdateData).subscribe(
      (data: any) => {
        if(data.message == "Your information has been sucessfully updated") {
          this.updateEditData.emit(newUpdateData);
          this.close();
        }
      }, (err) => {
        console.log("Error while updating user", err);
      }
    );
  }

  addUser(newAddUserData) {
    this.userService.addUser(newAddUserData).subscribe(
      (data: any) => {
        console.log(data);
        let message = data.message.substr(0,37);
        if(message ==  "Your information has been sucessfully") {
          this.updateEditData.emit(newAddUserData);
          this.close();
        }
      }, (err) => {
        console.log("Error while adding new user", err);
      }
    );
  }

  close() {
    this.Closebtn.nativeElement.click();
  }

  onSubmit(updatedData) {
    if(updatedData.user_id > 0) {
      this.updateUser(updatedData);
    } else {
      this.addUser(updatedData);
    }
  }
}
