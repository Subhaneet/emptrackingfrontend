import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserModal} from '../model/user';
import {UserService} from '../user.service';

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.css']
})

export class ViewProfileComponent implements OnInit {

  user: any;
  comparingData: any;

  @Input() set userData(specificUserData) {
    this.user = specificUserData;
    // this.userService.getAllUsers().subscribe(
    //   (res: any) => {
    //     res.data.filter( users => {
    //       if(users.user_email == this.user.user_email) {
    //         this.comparingData = users.user_name
    //       }
    //     } );
    //     console.log(res);
    //   }
    // )
  }

  @Output() viewUserProfile = new EventEmitter();

  constructor(private userService: UserService) { }


  ngOnInit() {
  }

}
