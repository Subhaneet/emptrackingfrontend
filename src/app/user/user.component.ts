import {Component, OnInit, ViewChild} from '@angular/core';
import {UserModal} from '../model/user';
import {UserService} from '../user.service';
import {UpdateModalComponent} from '../update-modal/update-modal.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})

export class UserComponent implements OnInit{

  @ViewChild(UpdateModalComponent, {static: false}) updateModal;

  user: any;
  showList = true;
  userInfo: UserModal;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getAllUsers();
  }

  getAllUsers(): any {
    this.userService.getAllUsers().subscribe((data:any) => {
        this.user = data.data;
      }
    );
  }

  addUser() {
    this.updateModal.isFormVisible = true;
    this.userInfo = new UserModal();
  }

  updateUser(userInfo) {
    this.updateModal.isFormVisible = true;
    this.userInfo = userInfo;
  }

  deleteUser(id) {
    if (window.confirm('Are you sure?') === true) {
      this.userService.deleteUser(id).subscribe(() => {
        let index = this.user.findIndex(x => x.user_id == id);
        if(index > -1){
         this.user.splice(index,1);
        }
      });
    }
  }

  toggleView() {
    this.showList = !this.showList;
  }

  handleAddOrEdit(updatedData) {
    let index = this.user.findIndex( x => x.user_id == updatedData.user_id );
    console.log("index from findindex", index);
    if(index >= 0) {
      this.user[index] = updatedData;
    }
    if(updatedData.user_id == undefined) {
      this.user = [...this.user, updatedData]
    }
  }
}
