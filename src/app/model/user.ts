export class UserModal {
  // public id?: number;
  // public name?: string;
  // public email?: string;
  // public role?: string;
  // public phone?: string;
  // public m_id?: number;
  constructor(public user_id?: number,
              public user_name?: string,
              public user_email?: string,
              public user_phone?: string,
              public user_password?: string,
              public user_role?: string,
              public active_user?: string,
              public Is_Admin?: string,
              public user_dob?: boolean,
              public user_gender?: boolean)
  {
    // this.id = _id;
    // this._name = _name;
    // this.email = _email;
    // this.role = _role;
    // this.phone = _phone;
    // this.m_id = _M_id;
  }
}
