import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import {UserComponent} from './user/user.component';
import {RouterModule} from '@angular/router';
import { NavbarComponent } from './Navbar/navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddUserComponent } from './add-user/add-user.component';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import { UpdateModalComponent } from './update-modal/update-modal.component';
import { LoginComponent } from './login/login.component';
import { AdminApprovalComponent } from './admin-approval/admin-approval.component';
import { RegisterComponent } from './register/register.component';
import { ViewProfileComponent } from './view-profile/view-profile.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    NavbarComponent,
    AddUserComponent,
    UpdateModalComponent,
    LoginComponent,
    AdminApprovalComponent,
    RegisterComponent,
    ViewProfileComponent
  ],
  entryComponents: [UpdateModalComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
