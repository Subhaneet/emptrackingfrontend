export class LoginModal {
  user_email: string;
  user_password: string;
  constructor(public _email: string,
              public _password: string) {
    this.user_email = _email;
    this.user_password = _password;
  }
}
