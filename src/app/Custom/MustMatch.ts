import {AbstractControl, FormGroup, ValidatorFn} from '@angular/forms';

export function mustMatch(passwordKey: string, confirmPasswordKey: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[passwordKey];
    const matchingControl = formGroup.controls[confirmPasswordKey];

    if(matchingControl.errors && !matchingControl.errors.mustMatch) {
      //to return if prior error is found
      return;
    }

    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
//   ValidatorFn {
//     return (control: AbstractControl): {[key: string]: boolean} | null => {
//     if(!control){
//       return null;
//       }
//     const password = control.get(passwordKey);
//     const confirmPassword = control.get(confirmPasswordKey);
//     if(!password.value || !confirmPassword.value) {
//       return null;
//     }
//
//     if(password.value !== confirmPassword.value) {
//       return { passwordMismatch: true }
//     }
//     return  null
//     }
// }
}

